package threadSample;

/**
 * Clase ThreadEjemploExtiende
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class ThreadEjemploExtiende extends Thread {
    
    public ThreadEjemploExtiende(String str) {
        super(str);
    }
    
    @Override
    public void run() {
        try {
            for (int i = 1; i <= 5; i++) {
                System.out.printf("\n%s iteracion %d.", this.getName(), i);
                Thread.sleep(i * 1000);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        System.out.printf("\n%s MURIENDO!!! porque llego al fin de run()", this.getName());
    }

} //Fin de clase ThreadEjemploExtiende
