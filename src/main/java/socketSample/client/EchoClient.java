package socketSample.client;

import java.io.*;
import java.net.*;

/**
 * Clase EchoClient
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class EchoClient {

    public static void main(String args[]) {
        try {
            /* Socket cliente = new Socket("direccionIP", puerto); */
            //Creamos un cliente socket en Localhost, puerto 1234
            Socket cliente = new Socket(InetAddress.getLocalHost(), 1234);

            //Obtenemos una referencia al inputStream del cliente
            //en este stream el cliente leera lo que el servidor le envia
            InputStream clientIn = cliente.getInputStream();
            
            //Obtenermos una referencia del outputStream del cliente
            //con este stream el cliente envia datos al servidor.
            OutputStream clientOut = cliente.getOutputStream();
            
            //Creamos un writer y un reader para cada stream
            PrintWriter pw = new PrintWriter(clientOut, true);
            BufferedReader br = new BufferedReader(new InputStreamReader(clientIn));
            
            //Pedimos al usuario que ingrese un mensaje
            BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Ingrese un mensaje a enviar al servidor: ");
            String mensajeUsuario = stdIn.readLine();
            
            //Escribimos el mensaje al servidor
            pw.println(mensajeUsuario);
            
            //Verificamos que nos responde el servidor
            System.out.println("Mensaje del servidor: ");
            String mensajeServidor = br.readLine();
            System.out.println(mensajeServidor);
            
            //Cerramos los flujos y el socket
            pw.close();
            br.close();
            cliente.close();
            
        } catch (ConnectException ce) {
            System.out.println("No puede conectarse al servidor. " + ce.getMessage());
            
        } catch (IOException ie) {
            System.out.println("I/O Error. " + ie.getMessage());
        }
        
    }
} //Fin de clase EchoClient
