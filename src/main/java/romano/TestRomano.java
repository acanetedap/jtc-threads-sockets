package romano;

/**
 * Clase TestRomano
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class TestRomano {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) throws NroInvalidoException {

        System.out.printf("\nDecimal %d en romano es %s", 15, ConversorRomano.decimal_a_romano(15));
        System.out.printf("\nDecimal %d en romano es %s", 258, ConversorRomano.decimal_a_romano(258));
        System.out.printf("\nDecimal %d en romano es %s", 2012, ConversorRomano.decimal_a_romano(2012));        

    } //Fin de main

} //Fin de clase TestRomano
